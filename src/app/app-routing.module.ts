import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { NavComponent } from "./nav/nav.component";

const routes: Routes = [
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full',
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'dashboard',
        component: NavComponent,
        children: [
            {
                path: '',
                loadChildren: () => import('./nav/nav.module').then(m => m.NavModule)
            }
        ]
    },
    {
        path: 'users',
        component: NavComponent,
        children: [
            {
                path: '',
                loadChildren: () => import('./modules/users/users.module').then(m => m.UsersModule)
            }
        ]
    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }