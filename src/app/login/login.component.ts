import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { apiResponsObj } from '../models/apiResponse';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  emailId!: string;
  password!: string;

  constructor(private authenticate: AuthenticationService, private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  onLogin() {
    if (this.emailId && this.password) {
      this.authenticate.onUserLogin(this.emailId, this.password).subscribe((response) => {
        if(response.success) {
          this.snackBar.open(response.message, 'ok')
          this.router.navigate(['/dashboard'])
        } else {
          this.snackBar.open(response.message, 'ok')
        }
      })
    } else {
      this.snackBar.open("Enter Email and Password", 'ok')
    }
  }

}
