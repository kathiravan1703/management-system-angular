import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Users } from 'src/app/models/users';
import { UsersService } from 'src/app/services/users/users.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, AfterViewInit {

  userList = new Array;
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'emailId'];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  dataSource = new MatTableDataSource<Users>();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(private userService: UsersService) { }

  ngOnInit(): void {
    this.getAllUsers()
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort
  }

  getAllUsers() {
    this.userService.getAllUsers().subscribe(response => {
      if (response) {
        this.dataSource.data = response.payload;
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort
      }
    })
  }

}
