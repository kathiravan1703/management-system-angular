import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { apiResponsObj } from 'src/app/models/apiResponse';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  apiRoute: string = environment.apiURL

  constructor(private httpClient: HttpClient) { }

  contactAPIWithoutToken = (endpoint: string, httpverb: string, data?: any): Observable<apiResponsObj> => {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      })
    };

    if (httpverb == 'get') {
      return this.httpClient.get<apiResponsObj>(endpoint, httpOptions);
    } else {
      return this.httpClient.post<apiResponsObj>(endpoint, data, httpOptions);
    }

    // switch (httpverb) {
    //   case 'get': return this.httpClient.get<apiResponsObj>(endpoint);
    //   case 'post': return this.httpClient.post<apiResponsObj>(endpoint, data, httpOptions);
    // }
  }
}
