import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Users } from 'src/app/models/users';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  apiRoute = environment.apiURL

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<any> {
    return this.http.get<Users>(`${this.apiRoute}/users`)
  }
}
