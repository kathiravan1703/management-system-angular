import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { apiResponsObj } from '../models/apiResponse';
import { ConfigService } from './config/config.service';

const httpOptionsPlain = {
  headers: new HttpHeaders({
    'Accept': 'text/plain',
    'Content-Type': 'text/plain'
  }),
  'responseType': 'text'
};

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  apiRoute: string = this.config.apiRoute

  constructor(private config: ConfigService) { }

  onUserLogin(emailId: string, password: string) {
    return this.config.contactAPIWithoutToken(`${this.apiRoute}/login`, 'post', JSON.stringify({ emailID: emailId, password: password }))
  }
}
